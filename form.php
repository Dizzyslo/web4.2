<html lang="ru">
<head>
<title>Web 4</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style>
.error {
border: 2px solid red;
}
</style>
</head>
<body class="bg-secondary">
	<div class="bg-white w-50 mx-auto my-5 rounded">
	  <div class="w-75 mx-auto my-3">
		<form action="" method="POST">

			<label>
			<div>Name:</div>
			<input name="Name" <?php if ($errors['Name']) {
                print 'class="error"';}?> type="text"
				value="<?php print $values['Name'];?>"/>
			</label><br />

			<label>
			e-mail:<br />
			<input name="EMail" <?php if ($errors['EMail']) {
                print 'class="error"';}?> type="text"
				value="<?php print $values['EMail'];?>"/>
			</label><br />

			<label>
			Year:<br />
			<input name="Year" <?php if ($errors['Year']) {
                print 'class="error"';}?> type="text"
				value="<?php print $values['Year'];?>"/>
			</label><br />
			
            <div <?php if($errors['Sex']){
                print 'class="error mb-1"';}?>>
				Sex:<br />
				<label><input type="radio" name="Sex" value="Male" 
				    <?php if($values['Sex'] == "Male"){
				    print 'checked = "checked"';} ?>/> Male </label>
				<label><input type="radio" name="Sex" value="Female" 
				    <?php if($values['Sex'] == "Female"){
				    print 'checked = "checked"';}?> /> Female </label>
            </div>
            <div <?php if($errors['KolKon']){
                print 'class="error"';}?>>
            	<div>Kolichesto konechnostey:</div>
            	
				<label><input type="radio" name="KolKon" value="1" 
					<?php if($values['KolKon'] == "1"){
				    print 'checked = "checked"';}?>/> 1 </label>
				
				<label><input type="radio" name="KolKon" value="2" 
					<?php if($values['KolKon'] == "2"){
				    print 'checked = "checked"';}?>/> 2 </label>
				
				<label><input type="radio" name="KolKon" value="3" 
					<?php if($values['KolKon'] == "3"){
				    print 'checked = "checked"';}?>/> 3 </label>
				    
				<label><input type="radio" name="KolKon" value="4" 
					<?php if($values['KolKon'] == "4"){
				    print 'checked = "checked"';}?>/> 4 </label>
				    <br />    
 			</div>
			<label> Sposobnosti:<br />
				<select name="Sposobnosti[]" size="6" multiple="">
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='Ability to create a Rasengan') print 'selected=""'?>>Ability to create a Rasengan</option>
				    
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='Running speed is greater than that of Sonic') print 'selected=""'?>>Running speed is greater than that of Sonic</option>
					
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='Ability to split fire') print 'selected=""'?>>Ability to split fire</option>
						
					
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='Learning to teleport') print 'selected=""'?>>Learning to teleport</option>
						
					
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='Invisibility') print 'selected=""'?>>Invisibility</option>
						
					
					<option <?php for ($i=0; $i<count($values, COUNT_RECURSIVE)-8; $i++)
                        if ($values['Sposobnosti'][$i]=='One-Punch Man') print 'selected=""'?>>One-Punch Man</option>
						
				</select>
			</label><br />

			<label>Boigrafy:<br />
				<textarea name="Biog" <?php if ($errors['Biog']) {
                print 'class="error"';}?> type="text"
			    ><?php print $values['Biog'];?></textarea>
			</label><br />

			<br />
			<label <?php if ($errors['Checkbox']) {
                print 'class="error"';}?>>
                <input type="checkbox" name="Checkbox" value="Yes"
                	<?php if($values['Checkbox'] == "Yes"){
				    print 'checked = ""';}?>> Oznakomlen with instuction </label><br />

			<button type="submit" class="btn btn-dark mb-2">Send</button>

		</form>
	  </div>	
	</div>
</body>
</html>